---
title: Wait free synchronisation
author: Justus Adam
date: 2016-06-15
---

# Recap

## A concurrent system

. . .

program

:   sequence of instructions

process

:   independent calculation unit which executes the program

concurrent data object

:   data object implementation with shared data access by multiple threads

<div class="notes">
- process is not a unix process, more a thread
- I use process and thread synonymously
- data object implementation is the object itself plus a set of methods/actions on the object
</div>

---

A concurrent system consists of

- a set of processes
- a set of objects
- a set of actions on the shared objects

## Consensus protocol

*n*-consensus:

> - A set of *n* processes communicating through a set of objects
> - each process decides on a value *v* after a finite number of steps
> - each process must decide on the same value *v*
> - *v* must have been input to one of the processes

<div class="notes">
In sequential execution this is solved with the *sticky bit*, all `decide()` operations return the value of the first `decide()`
</div>

---

### States

> - n-valent
> - univalent
> - bivalent

## Consensus number

The highest number *n* for which an object can solve *n*-consensus is the objects consensus number.

If the object solves *n* consensus for any n < 0, then its consensus number is infinite.

# A concurrent problem

---

A wait-free implementation of a concurrent data object is one that guarantees that any process can complete any operation in a finite number of steps, regardless of the execution speeds of the other processes.

<div class="notes">
Back then largely lock based synchronisation
</div>

---

We want to design concurrent data objects *O* such that:

- any operation on *O* finishes after a finite number of steps
- no operation requires a certain action to be performed on *O* by a process other than the process which started the original operation

<div class="notes">
- the second one is so that we do not depend on another process to make some condition true, called *busy-waiting*
- locks are an example for busy waiting
</div>

. . .

We call this object *wait-free*

---

An object is *bounded wait-free* if there is an *N* such that no operation takes more than *N* steps to complete.

. . .

Any *bounded wait-free* is also *wait-free*, but not every *wait-free* object is *bounded wait-free*.

---

Synchronisation

:   Ensuring a set of parallel threads agree on an ordering and effect of a sequence of performed actions

# Hierarchy of wait free objects

<div class="notes">
bla bla
</div>

---

A concurrent object *X* can be implemented by a concurrent object *Y*

. . .

If *X* is wait-free, an implementation by *Y* must also be wait-free.

<div class="notes">
- This is analogous to the adapter pattern in java
</div>

---

If *Y* implements *X* wait-free and *X* has consensus number *n*, *Y* must also have at least consensus number *n*.

Intuitively consensus means there is **a way** to solve *n* consensus. If *Y* implements *X* and *X* solves *n* consensus then that implementation is **the way** in which *Y* solves *n* consensus.

## Atomic Read/Write registers

```haskell
read :: RWRegister a -> IO a
write :: RWRegister a -> a -> IO ()
```

. . .

- Suppose we have a two process consensus protocol for rw-registers
- run until both next step for either processes results in distinct univalent state

<div class="notes">
This is the only proof I'll step through. Just to give you an idea of the methodology.
</div>

---

1. Read from the shared register cannot change the decision value of **both** processes
2. Write to different registers will not work because the order is not significant
3. Write to the same register one process will overwrite the other hence the action of Q is insignificant

We conclude atomic read-write registers have consensus number 1.

---

![](images/rw-register-proof.png)

---

![](images/rw-register-proof-2.png)

## RMW register

```haskell
modify :: RMWRegister a -> (a -> a) -> IO a
fetchAndInc :: RMWRegister Int -> IO Int
```

. . .

- more powerful than read/write registers
- consensus number of 2
- cannot be used to implement objects with consensus number 3

## Compare and swap register

```haskell
newCASRegister :: Eq a => a -> IO (CASRegister a)
compareAndSwap :: Eq a => CASRegister a -> a -> a -> IO a
```

- `compareAndSwap register old new` compares current `register` value with `old` and if equal sets value to `new`, always returns the old value from the register
- consensus number ∞

---

```haskell
execProcess :: CASRegister (Maybe Int) -> Int -> IO Int
execProcess casRegister myNumber = do
    val <- compareAndSwap casRegister Nothing (Just myNumber)
    case val of
        Nothing -> myNumber -- we wrote first
        Just otherNr -> otherNr -- someone else wrote 'otherNr' already

main :: IO ()
main = do
    casRegister <- newCASRegister Nothing
    processes <- for [1..10] (async . execProcess casRegister)
    decisions <- for processes wait
    assert $ length (nub decisions) == 1
```

<div class="notes">
- solves n-consensus by
    - initialising register to ⏊
    - each process calls compare&swap
    - first call to succeed places its value in the register
    - each process decides on the value returned (unless ⏊)
</div>


## Other examples

| object | consensus number |
|--------|:----------------:|
| FIFO queue | 2 |
| Augmented queue (FIFO + `peek()` operation) | ∞ |
| mem-to-mem operations | ∞ |
| *m*-register assignment | 2*m*-2 |

---

No object of infinite consensus number can be constructed from objects with a finite consensus number.

# Universal objects

---

A universal object can be used to implement any other object.

. . .

Any object with consensus number *n* is universal in a system with *n* or less processes.

## Construction

> - linked list of actions
> - action values are stored in a consensus object
> - backlinks are stored in a consensus object
> - processes try to figure out which action should be performed first

# Questions

---

What is a concurrent data object?

. . .

A shared piece of data and a set of operations on it.

---

Name one condition which a wait free object must satisfy.

. . .

1. any operation on *O* finishes after a finite number of steps
2. no operation requires a certain action to be performed on *O* by a process other than the process which started the original operation

---

What does consensus number 3 mean?

. . .

An object with the consensus number 3 solves the consensus problem for at most 3 concurrent processes.

---

What is the consensus number of atomic read-write registers?

. . .

1

---

Name a concurrent data object with an infinite consensus number.

. . .

- Compare and Swap register
- Augmented queue (FIFO + Peek)
- Memory to Memory operations

---

What does it mean to be a universal object?

. . .

A universal object implements any other object.

---

# Appendix

## Sources

- *Herlihy, Maurice.* "Wait-free synchronization." ACM Transactions on Programming Languages and Systems (TOPLAS) 13.1 (1991): 124-149.
- *Herlihy, Maurice, and Nir Shavit.* "The art of multiprocessor programming." PODC. Vol. 6. 2006.

## Slides

Created with [reveal.js](http://lab.hakim.se/reveal-js/) and [pandoc](http://pandoc.org).

Hosted:

[static.justus.science/presentations/wait-free-sync/](http://static.justus.science/presentations/wait-free-sync/)

Sources:

[gitlab.com/JustusAdam/presentation-wait-free-sync](https://gitlab.com/JustusAdam/presentation-wait-free-sync)
